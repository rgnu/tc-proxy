# HTTP/Socks4/Socks5 proxy with Traffic Shapping support

`  
  Usage: server [options]


  Options:

    -p, --port <port>       listen port, default 1099
    -d, --downstream <Bps>  downstream bandwidth, default 32KBps
    -u, --upstream <Bps>    upstream bandwidth, default equal to downstream
    -l, --latency <ms>      network latency, default to 50ms
    -g, --profile-gprs      GPRS profile up:20kbps, down:50kbps latency:500ms
    -2, --profile-2g        2G Good profile up:150kbps, down:450kbps latency:150ms
    -3, --profile-3g        3G Good profile up:750kbps, down:1.5Mbps latency:40ms
    -h, --help              output usage information
`

