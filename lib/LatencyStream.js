var stream   = require('stream');

function LatencyStream(time) {
    var ts    = stream.Transform();
    var sleep = true;

    ts._transform = function (data, encoding, done) {
        var that = this;

        if (sleep) {
          setTimeout(function() {
            that.push(data);
            done();
          }, time);
          sleep = false 
        } else {
          that.push(data);
          done();
        }
    }

    return ts;
}


module.exports = LatencyStream;
