
module.exports = (function() {
    'use strict';

    var util = require('util');

    function Logger(level, label) {
        this.level = level || 2;
        this.label = label || '[Logger]';
        this.init();
    }

    Logger.prototype.setLevel = function setLevel(level) {
        this.level = level;
    };

    Logger.prototype.getLevel = function getLevel() {
        return this.level;
    };

    Logger.prototype.setLabel = function setLabel(label) {
        this.label = label;
    };

    Logger.prototype.init = function init() {
        this._log     = console.log.bind(console);
        console.log   = this.debug.bind(this);
        console.info  = this.info.bind(this);
        console.warn  = this.warn.bind(this);
        console.error = this.error.bind(this);
    };

    Logger.prototype.log = function (level, msg) {
        this._log('%s [%s] %s', this.label, level, msg.replace(/\r?\n|\r/g, ' '));
    };

    Logger.prototype.error = function () {
        if (this.level >= 0) {
            this.log('ERROR', util.format.apply({}, arguments));
        }
    };

    Logger.prototype.warn = function () {
        if (this.level >= 1) {
            this.log('WARN ', util.format.apply({}, arguments));
        }
    };

    Logger.prototype.info = function () {
        if (this.level >= 2) {
            this.log('INFO ', util.format.apply({}, arguments));
        }
    };

    Logger.prototype.debug = function () {
        if (this.level >= 3) {
            this.log('DEBUG', util.format.apply({}, arguments));
        }
    };

    return Logger;
}());
