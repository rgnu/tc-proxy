var net      = require('net');
var Proxy    = require('proxy.stream').Proxy;
var throttle = require('stream-throttle');
var opts     = require('commander');
var util     = require('util');
var lib      = require('./lib')


function runProxy(port, downRate, upRate, latency) {

  var tgUpstream    = new throttle.ThrottleGroup({rate: upRate, chunksize: upRate});
  var tgDownstream  = new throttle.ThrottleGroup({rate: downRate, chunksize: downRate});
  var Logger        = new lib.Logger(2, '[Tc-Proxy]');

  var server        = net.createServer(function(s) {

    var proxy         = new Proxy();
    var latencyStream = lib.LatencyStream(latency);

    s.on('error', function(err) {
      console.error(err);
      s.destroy();
    })

    s
    .pipe(latencyStream)
    .pipe(tgUpstream.throttle())
    .pipe(proxy)
    .pipe(tgDownstream.throttle())
    .pipe(s);
  })

  server.listen(port, function() {
    console.info('HTTP/Socks4/Socks5 proxy listening on port %d [Up:%dB/s] [Down:%dB/s] [Latency:%dms]', port, upRate, downRate, latency);
  });
}


function toInteger(s) {
  return Number.parseInt(s)
}

function main() {

    opts
    .option('-p, --port <port>', 'listen port, default 1099', toInteger, 1099)
    .option('-d, --downstream <Bps>', 'downstream bandwidth, default 32KBps', toInteger, 32 * 1024)
    .option('-u, --upstream <Bps>', 'upstream bandwidth, default equal to downstream', toInteger)
    .option('-l, --latency <ms>', 'network latency, default to 50ms', toInteger, 50)
    .option('-g, --profile-gprs', 'GPRS profile up:20kbps, down:50kbps latency:500ms', {upstream:2560, downstream:6400, latency:500})
    .option('-2, --profile-2g', '2G Good profile up:150kbps, down:450kbps latency:150ms', {upstream:19200, downstream:57600, latency:150})
    .option('-3, --profile-3g', '3G Good profile up:750kbps, down:1.5Mbps latency:40ms', {upstream:96000, downstream:196608, latency:40})
    .parse(process.argv);

    opts.profileGprs && util._extend(opts, opts.profileGprs)
    opts.profile2g && util._extend(opts, opts.profile2g)
    opts.profile3g && util._extend(opts, opts.profile3g)


    if (opts.upstream === undefined)
        opts.upstream = opts.downstream;

    runProxy(opts.port, opts.upstream, opts.downstream, opts.latency);
}

main();



